/** 
* @projectDescription    WHR Calculator - a funny body mass index calculator
*
* @author   Roccoor Multimedia
* @version  2.1
*/

// Create namespace ///////////////////////////////////////////////////////////
if (!window.com) com = {};
if (!com.roccoor) com.roccoor = {};
if (!com.roccoor.whr) com.roccoor.whr = {};

// Functions //////////////////////////////////////////////////////////////////

/**
 * Displays the calculated WHR. This function need to be called from onscreanready or ondomready event.
 * @alias evaluateWhr
 * @alias com.roccoor.whr.evaluateWhr
 * @param {Object} element BBUI.js screen element. Use this parameter when invoking from onscreenready.
 * @sample 
 * onscreenready: function(element, id) {
 *											if (id == 'screenWhr') {
 *												com.roccoor.whr.evaluateBmi(element);
 *											}
 *										}
 *
 * ondomready:    function(element, id) {
 *											if (id == 'screenWhr') {
 *												com.roccoor.whr.initialize_screenBmi();
 *											}
 *										}
 */
com.roccoor.whr.evaluateBmi = function(element) {
	// Format the WHR number nicely
	var whr = new Number(window.whrData.whr);
	whr = com.roccoor.whr.formatDecimal(window.whrData.whr, 2);
	window.whrData.whr = whr;
	
	//alert(window.whrData.gender);
	
	var messageResult;
	var messageDescription;
	var image;
	if ('male' === window.whrData.gender) {
		if (window.whrData.whr <= 0) {
			messageResult = 'ERROR!!!';
			messageDescription = 'You have entered invalid numbers! Please try again!';
			image = 'images/balls/0-redcross.png';
		}
		else if (window.whrData.whr <= 0.85) {
			messageResult = 'Your waist-to-hip ratio is exelent!';
			messageDescription = 'If your waist to hip ratio is below 0.85, your health risks statistically related to waist to hip ratio are minimal as it\'s unlikely that carrying much central fat. This is not to say that you aren�t overweight - you are if you have excess fat deposition distributed all over your body you\'re still at risk of disease.';
			image = 'images/balls/1-green.png';
		}
		else if (window.whrData.whr <= 0.9) {
			messageResult = 'Your waist-to-hip ratio is good!';
			messageDescription = 'Generally if you have a ratio of 0.85 to 0.9 you\'re at low risk of weight-related disease but it\'s important to maintain your shape by a healthy diet and regular exercise. The longer you can prevent central fat deposition, the longer and healthier your life will be.';
			image = 'images/balls/2-yellowgreen.png';
		}
		else if (window.whrData.whr <= 0.95) {
			messageResult = 'Your waist-to-hip ratio is average!';
			messageDescription = 'You may feel you\'re healthy when you have a ratio of 0.9 to 0.95 but you already will have some fat deposited around the organs and in the membranes called the �omentum� that line the abdominal cavity. This fat will have started to make the hormones and chemicals which increase your risk of disease.';
			image = 'images/balls/3-yellow.png';
		}
		else if (window.whrData.whr <= 1) {
			messageResult = 'Your waist-to-hip ratio is high!';
			messageDescription = 'A high ratio of 0.95 to 1 shows that you have centrally stored fat and are at increased risk of disease compared to someone with no central fat. You may have no symptoms yet of diseases such as blood vessel inflammation and hardening which lead to stroke, high blood pressure or heart disease, or the hormonal affects such as infertility or pre-cancerous tissue change, but the unseen damage is already being done. It\'s important to stop that damage before it worsens - by the time you have symptoms, damage is already severe.';
			image = 'images/balls/4-orange.png';
		}
		else {
			messageResult = 'Your waist-to-hip ratio is extreme!';
			messageDescription = 'With a very large abdominal girth compared to your hips you\'re extremely likely to be holding dangerous amounts of fat centrally, and so be at high risk of the all the diseases associated with this. In particular heart disease, high blood pressure and diabetes develop when the inflammatory effects of that mass of fat hit your blood vessels. Also, fat deposits in the liver, a condition called fatty liver, lead to the swelling and tenderness of Non-Alcoholic Steatohepatitis (NASH), and eventually a type of non-alcoholic cirrhosis. Generally men are more likely to get an overhanging �gut� but women�s livers are actually more sensitive to damage from fat and also alcohol.</br><br>Your lifespan will be shortened by several years unless you do something about your diet and lifestyle.';
			image = 'images/balls/5-red.png';
		}
	}
	else {
		if (window.whrData.whr <= 0) {
			messageResult = 'ERROR!!!';
			messageDescription = 'You have entered invalid numbers! Please try again!';
			image = 'images/balls/0-redcross.png';
		}
		else if (window.whrData.whr <= 0.75) {
			messageResult = 'Your waist-to-hip ratio is exelent!';
			messageDescription = 'If your waist to hip ratio is below 0.75, your health risks statistically related to waist to hip ratio are minimal as it\'s unlikely that carrying much central fat. This is not to say that you aren�t overweight - you are if you have excess fat deposition distributed all over your body you\'re still at risk of disease.';
			image = 'images/balls/1-green.png';
		}
		else if (window.whrData.whr <= 0.8) {
			messageResult = 'Your waist-to-hip ratio is good!';
			messageDescription = 'Generally if you have a ratio of 0.75 to 0.8 you\'re at low risk of weight-related disease but it\'s important to maintain your shape by a healthy diet and regular exercise. The longer you can prevent central fat deposition, the longer and healthier your life will be.';
			image = 'images/balls/2-yellowgreen.png';
		}
		else if (window.whrData.whr <= 0.85) {
			messageResult = 'Your waist-to-hip ratio is average!';
			messageDescription = 'You may feel you\'re healthy when you have a ratio of 0.8 to 0.85 but you already will have some fat deposited around the organs and in the membranes called the �omentum� that line the abdominal cavity. This fat will have started to make the hormones and chemicals which increase your risk of disease.';
			image = 'images/balls/3-yellow.png';
		}
		else if (window.whrData.whr <= 0.9) {
			messageResult = 'Your waist-to-hip ratio is high!';
			messageDescription = 'A high ratio of 0.85 to 0.9 shows that you have centrally stored fat and are at increased risk of disease compared to someone with no central fat. You may have no symptoms yet of diseases such as blood vessel inflammation and hardening which lead to stroke, high blood pressure or heart disease, or the hormonal affects such as infertility or pre-cancerous tissue change, but the unseen damage is already being done. It\'s important to stop that damage before it worsens - by the time you have symptoms, damage is already severe.';
			image = 'images/balls/4-orange.png';
		}
		else {
			messageResult = 'Your waist-to-hip ratio is extreme!';
			messageDescription = 'With a very large abdominal girth compared to your hips you\'re extremely likely to be holding dangerous amounts of fat centrally, and so be at high risk of the all the diseases associated with this. In particular heart disease, high blood pressure and diabetes develop when the inflammatory effects of that mass of fat hit your blood vessels. Also, fat deposits in the liver, a condition called fatty liver, lead to the swelling and tenderness of Non-Alcoholic Steatohepatitis (NASH), and eventually a type of non-alcoholic cirrhosis. Generally men are more likely to get an overhanging �gut� but women�s livers are actually more sensitive to damage from fat and also alcohol.</br><br>Your lifespan will be shortened by several years unless you do something about your diet and lifestyle.';
			image = 'images/balls/5-red.png';
		}
	}
	
	if (!element) {
		document.getElementById('panel-headerResult').textContent = 'Your ratio: ' + window.whrData.whr.toString();
		document.getElementById('messageResult').textContent = messageResult;
		document.getElementById('messageDescription').innerHTML = messageDescription;
		document.getElementById('fatman').src = image;
	}
	else {
		element.getElementById('panel-headerResult').textContent = 'Your ratio: ' + window.whrData.whr.toString();
		element.getElementById('messageResult').textContent = messageResult;
		element.getElementById('messageDescription').innerHTML = messageDescription;
		element.getElementById('fatman').src = image;
	}
}

/**
 * Exits the application
 * @alias exit
 * @alias com.roccoor.whr.exit
 */
com.roccoor.whr.exit = function() {
	blackberry.app.exit();
}

/**
 * Opens BBM functionality screen
 * @alias shareOnBbm
 * @alias com.roccoor.whr.shareOnBbm
 */
com.roccoor.whr.shareOnBbm = function() {
	bb.pushScreen('screenShare.html', 'screenShare');
}

// Helper functions
/**
 * Formats a decimal
 * @alias formatDecimal
 * @alias com.roccoor.whr.formatDecimal
 * @param {Decimal} num        The number to be formatted
 * @param {Integer} numPlaces  The number of decimal places to be used for formatting the decimal number
 */
com.roccoor.whr.formatDecimal = function(num, numPlaces){
			var snum = new String(parseFloat(num))
			var i=z=0; 
			var sout=ch="" 
			while(i<snum.length && ch != '.' ){
			ch = snum.charAt(i++)
			sout+=ch
			}
			while(i<snum.length && z++<numPlaces){
				ch = snum.charAt(i++)
				sout+=ch
			}
			if(snum.indexOf('.')==-1)sout+='.';
			while(z++<numPlaces)sout+='0';
			return sout
}
