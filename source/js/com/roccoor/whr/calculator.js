/** 
* @projectDescription    WHR Calculator - a funny body mass index calculator
*
* @author   Roccoor Multimedia
* @version  2.1
*/

// Create namespace ///////////////////////////////////////////////////////////
if (!window.com) com = {};
if (!com.roccoor) com.roccoor = {};
if (!com.roccoor.whr) com.roccoor.whr = {};

// Global objects /////////////////////////////////////////////////////////////

// Create global whrData Object
// Self-executing anonymous function that creates whrData o
(function() {
	if (!window.whrData) window.whrData = new Object();
})();

// Functions //////////////////////////////////////////////////////////////////

/**
 * Intializes the global variable window.whrData.units
 * @alias initialize_screenInput
 * @alias com.roccoor.whr.initialize_screenInput
 */
com.roccoor.whr.initialize_screenInput = function() {
	window.whrData.units = 'imperial';
}

// pill-buttons functions - BEGIN
/**
 * Selects the input subscreen for "Imperial" units
 * @alias pillButton_selectImperial
 * @alias com.roccoor.whr.pillButton_selectImperial
 */
com.roccoor.whr.pillButton_selectImperial = function() {
	document.getElementById('label_Waist').textContent = 'Inches';
	document.getElementById('label_Hip').textContent = 'Inches';
}

/**
 * Selects the input subscreen for "Metric" units
 * @alias pillButton_selectMetric
 * @alias com.roccoor.whr.pillButton_selectMetric
 */
com.roccoor.whr.pillButton_selectMetric = function() {
	document.getElementById('label_Waist').textContent = 'cm';
	document.getElementById('label_Hip').textContent = 'cm';
}
// pill-buttons functions - END

// User input functions - BEGIN
/**
 * Reads the user input
 * @alias getData
 * @alias com.roccoor.whr.getData
 */
com.roccoor.whr.getData = function() {
	// Collect user input
	var waistCircumference = parseFloat(document.getElementById('inputNumber_WaistCircumference').value);
	var hipCircumference = parseFloat(document.getElementById('inputNumber_HipCircumference').value);
	
	var select_Gender = document.getElementById('select_Gender');
	var gender = select_Gender.options[select_Gender.selectedIndex].value;
	
	// Fix user input
	if (isNaN(waistCircumference) || waistCircumference < 0)
		waistCircumference = 0;
	if (isNaN(hipCircumference) || hipCircumference < 0)
		height_in = 0;
	
	// Save the collected data
	window.whrData.waistCircumference = waistCircumference;
	window.whrData.hipCircumference = hipCircumference;
	window.whrData.gender = gender;
}
// User input functions - END

/**
 * Calculates and stores the waist-to-hip ratio (WHR) in the global variable windows.whrData
 * @alias calculateWhr
 * @alias com.roccoor.whr.calculateWhr
 */
com.roccoor.whr.calculateWhr = function() {
		'use strict';
		
		com.roccoor.whr.getData();

		// Compute and save WHR
		var whr = window.whrData.waistCircumference / window.whrData.hipCircumference;
		if (isNaN(whr) || !isFinite(whr))
			whr = 0;
		window.whrData.whr = whr;
}

/**
 * Opens screen "screenResult"
 * @alias display_screenResult
 * @alias com.roccoor.whr.display_screenResult
 */
com.roccoor.whr.display_screenResult = function() {
	com.roccoor.whr.calculateWhr();
	bb.pushScreen('screenResult.html', 'screenResult');
}

/**
 * Opens screen "screenHelp"
 * @alias display_screenHelp
 * @alias com.roccoor.whr.display_screenHelp
 */
com.roccoor.whr.display_screenHelp = function() {
	bb.pushScreen('screenHelp.html', 'screenHelp');
}

// Helper functions
/**
 * Calculates logarithm of 10
 * @alias log10
 * @alias com.roccoor.whr.log10
 * @param {Interger} value Input number
 * @return {Integer} Logarithm of 10
 */
com.roccoor.whr.log10 = function(value) {
	return Math.log(value) / Math.LN10;
}
