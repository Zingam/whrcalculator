/** 
* @projectDescription    WHR Calculator - a funny body mass index calculator
*
* @author   Roccoor Multimedia
* @version  2.1
*/

// Create namespace ///////////////////////////////////////////////////////////
if (!window.com) com = {};
if (!com.roccoor) com.roccoor = {};
if (!com.roccoor.whr) com.roccoor.whr = {};

// Functions //////////////////////////////////////////////////////////////////

var registered = false;
var uuid = { uuid: '4d4e1e00-7d35-11e2-9e96-0800200c9a66' };

/*
	4d4e1e00-7d35-11e2-9e96-0800200c9a66
	4d4e1e01-7d35-11e2-9e96-0800200c9a66
	4d4e1e02-7d35-11e2-9e96-0800200c9a66
	4d4e1e03-7d35-11e2-9e96-0800200c9a66
	4d4e1e04-7d35-11e2-9e96-0800200c9a66
*/

/**
 * Register the application to access the BBM Social Platform 
 * @alias register
 * @alias com.roccoor.whr.register
 */
com.roccoor.whr.register = function() {
	if (!registered)
		blackberry.event.addEventListener('onaccesschanged', com.roccoor.whr.accessChangedCallback);
}

/**
 * Callback function to register the application with BBM used by com.roccoor.whr.register
 * @alias accessChangedCallback
 * @alias com.roccoor.whr.accessChangedCallback
 */
com.roccoor.whr.accessChangedCallback = function(accessible, status) {
	if (status === 'allowed')
		// Access is allowed
		registered = accessible;
	else if (status === 'unregistered')
		// Register the application with BBM Social Platform
		blackberry.bbm.platform.register(uuid);
	else if (status === 'registered')
		alert('Access is pending and being processed.');
	else if (status === 'user')
		alert('Access is blocked by user');
	else if (status === 'rim')
		alert('Access is blocked by RIM');
	else if (status === 'nodata')
		alert('Access is blocked because the device is out of data coverage. A data connection is required to register application.');
	else if (status === 'unexpectederror')
		alert('Access is blocked because an unexpected error has occured.');
	else if (status === 'invaliduuid')
		alert('Access is blocked because an invalid UUID was provided when registering.');
	else if (status === 'temperror')
		alert('Access is blocked because of temporary error. Try again later.');
}

/** 
 * Invite BBM contacts to download the application
 * @alias inviteToDownload
 * @alias com.roccoor.whr.inviteToDownload
 */
com.roccoor.whr.inviteToDownload = function() {
	if(registered)
		blackberry.bbm.platform.users.inviteToDownload();
	else
		alert('Application is not yet registered. Please try again!');
}

/**
 * Shares WHR with selected contacts over BBM with a cool message.
 * @alias shareWhr
 * @alias com.roccoor.whr.shareWhr
 */
com.roccoor.whr.shareWhr = function() {		 
	// Create a cool massage to send
	var message;
	
	if ('male' === window.whrData.gender) {
		if (window.whrData.whr <= 0) {
			message = 'ERROR!!! Invalid WHR value!';
		}
		else if (window.whrData.whr <= 0.85) {
			message = 'My waist-to-hip ratio is exelent! WHR: ' + window.whrData.whr.toString();
		}
		else if (window.whrData.whr <= 0.9) {
			message = 'My waist-to-hip ratio is good! WHR: ' + window.whrData.whr.toString();
		}
		else if (window.whrData.whr <= 0.95) {
			message = 'My waist-to-hip ratio is average! WHR: ' + window.whrData.whr.toString();
		}
		else if (window.whrData.whr <= 1) {
			message = 'My waist-to-hip ratio is high! WHR: ' + window.whrData.whr.toString();
		}
		else {
			message = 'My waist-to-hip ratio is extreme! WHR: ' + window.whrData.whr.toString();
		}
	}
	else {
		if (window.whrData.whr <= 0) {
			message = 'ERROR!!! Invalid WHR value! WHR: ' + window.whrData.whr.toString();
		}
		else if (window.whrData.whr <= 0.75) {
			message = 'My waist-to-hip ratio is exelent! WHR: ' + window.whrData.whr.toString();
		}
		else if (window.whrData.whr <= 0.8) {
			message = 'My waist-to-hip ratio is good! WHR: ' + window.whrData.whr.toString();
		}
		else if (window.whrData.whr <= 0.85) {
			message = 'My waist-to-hip ratio is average! WHR: ' + window.whrData.whr.toString();
		}
		else if (window.whrData.whr <= 0.9) {
			message = 'My waist-to-hip ratio is high! WHR: ' + window.whrData.whr.toString();
		}
		else {
			message = 'My waist-to-hip ratio is extreme! WHR: ' + window.whrData.whr.toString();
		}
	}
		
	var invokeObject = { target:   'sys.bbm.sharehandler',
						 action:   'bb.action.SHARE',
						 mimeType: 'text/plain',
						 data:     message
					   }
	
	// Send the message
	if (registered)
		blackberry.invoke.invoke(invokeObject);
	else
		alert('Application not yet registered! Please try again!');
}

