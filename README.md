WHRCalculator
=============

Current build: 1.0.0.1

Introduction
-------------
Waist-hip ratio or waist-to-hip ratio (WHR) is the ratio of the circumference
of the waist to that of the hips.

Formula
-------

WHR = Waist / Hip


Revisions
=========

Version 1.0
-----------
 + Supports: BBM integration


License notes
=============

Graphic assets
--------------
The graphic assets are not free and therefore it is not allowed to be 
downloaded, copied, redistributed or used in any other way.
